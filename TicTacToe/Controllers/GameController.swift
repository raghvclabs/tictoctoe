//
//  ViewController.swift
//  TicTacToe
//
//  Created by Click Labs on 2/2/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class GameController: UIViewController {
  var gameStateArray = [0,0,0,0,0,0,0,0,0]   // 0 = empty ,  2 = cross ,1 = naught
  var winner = 0
  var winMessage:String!
  var numberOfButtonTap = 0
  var tie = 0  // number of tie
  var won1 = 0 // number of games won by player 1
  var won2 = 0 // number of games won by player 2
  
  @IBOutlet weak var noOfTie: UILabel!
  @IBOutlet weak var wonByPlayer2: UILabel!
  @IBOutlet weak var wonByPlayer1: UILabel!
  @IBOutlet weak var player2Label: UILabel!
  @IBOutlet weak var player1Label: UILabel!
  
  @IBOutlet weak var scoreBoardView: UIView!
  @IBOutlet weak var turnTitle: UINavigationItem!
  @IBOutlet weak var bar: UINavigationBar!
  // MARK:- SaveScore button in ScoreCard View
  @IBAction func saveScore(sender: AnyObject) {
    self.showRecord()
  }
  
  func showRecord(){
    let Records = "\(player1Label.text!) vs \(player2Label.text!)         \(wonByPlayer1.text!)-\(wonByPlayer2.text!)       \(noOfTie.text!)"
    gameRecords.append(Records)
    let storedItem = gameRecords
    NSUserDefaults.standardUserDefaults().setObject(storedItem, forKey: "gameRecords")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
  
  @IBAction func Return(sender: AnyObject) {
    scoreBoardView.alpha = 0  //hide the scoreboard
  }
  
  // MARK:- RESET BUTTON FUNCTION IN ALERT
  func reset() {
    bar.hidden = false
    gameStateArray = [0,0,0,0,0,0,0,0,0]
    winner = 0
    numberOfButtonTap = 0
    var button :UIButton   // delete the image on the buttons
    for index in 1...9 {
      button  = view.viewWithTag(index) as UIButton
      button.setImage(nil, forState: .Normal)
    }
    //MARK :- Setting the message for turn of the player
    if turn%2 == 0 {
      turnTitle.title = "\(player1Name) first turn"
    } else {
      turnTitle.title = "\(player2Name) first turn"
    }
  }
  
  //MARK:- game operation code,displaying cross n naught etc
  @IBAction func buttonTapped(sender: AnyObject) {
    player1Label.text = player1Name
    player2Label.text = player2Name
    bar.hidden = true
    if gameStateArray[sender.tag-1] == 0 && winner == 0 {
      numberOfButtonTap++
      var image = UIImage()
      if turn%2 == 1  {  // to change the turn
        image = UIImage( named: "X.png")!
        gameStateArray[sender.tag-1] = 2
      } else {
        image = UIImage( named: "O.png")!
        gameStateArray[sender.tag-1] = 1
      }
      
      // Main Logic of checking winner
      for combination in WinningCombination {
        if gameStateArray[combination[0]] == gameStateArray[combination[1]] &&  gameStateArray[combination[1]] ==  gameStateArray[combination[2]] &&  gameStateArray[combination[0]] != 0 {
            winner = gameStateArray[combination[0]]    //value 0,1,2
        }
      }
      
      if winner != 0 {
        if winner == 1 {
          winMessage = "\(player1Name) has won ".capitalizedString
          ++won1
          wonByPlayer1.text = String(won1)
        } else {
          ++won2
          winMessage = "\(player2Name) has won ".capitalizedString
          wonByPlayer2.text = String(won2)
        }
      
        // MARK :- alert for show result
        var alert1 = UIAlertController(title: "RESULT", message: winMessage + "😄😄", preferredStyle: UIAlertControllerStyle.Alert)
        alert1.addAction(UIAlertAction(title: "SCOREBOARD", style: UIAlertActionStyle.Default,handler : {(alert:UIAlertAction!) -> Void in
          self.scoreBoardView.alpha = 1
          self.reset()
        }))
        alert1.addAction(UIAlertAction(title: "PlayAgain", style: UIAlertActionStyle.Default,handler :{(alert:UIAlertAction!) -> Void in
          self.reset()
        }))
        self.presentViewController(alert1, animated: true, completion: nil)
        // for tie condition
      } else if winner == 0 &&  numberOfButtonTap == 9 {
        ++tie
        noOfTie.text = String(tie )
        var alert2 = UIAlertController(title: "RESULT", message: "OOPS Its Tie", preferredStyle: UIAlertControllerStyle.ActionSheet)
        alert2.addAction(UIAlertAction(title: "PlayAgain", style: UIAlertActionStyle.Default,handler :
              {(alert:UIAlertAction!) -> Void in
               self.reset()
        }))
        alert2.addAction(UIAlertAction(title: "SCOREBOARD", style: UIAlertActionStyle.Default,handler :{(alert:UIAlertAction!) -> Void in
             self.scoreBoardView.alpha = 1
             self.reset()
        }))
        self.presentViewController(alert2, animated: true, completion: nil)
       }
      turn++
      sender.setImage(image, forState: .Normal)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    scoreBoardView.alpha = 0
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

