//
//  MyViewController.swift
//  TicTacToe
//
//  Created by Click Labs on 2/4/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class UserInformationController: UIViewController,UITextFieldDelegate {
  
  func textField(textField: UITextField!, shouldChangeCharactersInRange range: NSRange, replacementString string: String!) -> Bool {
    let newLength = countElements(textField.text!) + countElements(string!) - range.length
    return newLength <= 10 //restrict the text filed length to 8
  }
  
  @IBOutlet weak var player2NametextField: UITextField!
  @IBOutlet weak var player1NametextField: UITextField!
  
  @IBOutlet weak var segmentedControl: UISegmentedControl!
  
  @IBAction func playButton(sender: AnyObject) {
    if !player1NametextField.text.isEmpty {
      player1Name = player1NametextField.text.capitalizedString
    }
    if !player2NametextField.text.isEmpty {
      player2Name = player2NametextField.text.capitalizedString
    }
  }
  
  @IBAction func chooseOption(sender: AnyObject) {  //Player 1 selecting cross or naught
    switch segmentedControl.selectedSegmentIndex {
      case 0:
        turn = 1
      case 1:
        turn = 2
      default:
        break;
    }
  }
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) { // func to hide keyboard on touching outside
    self.view.endEditing(true)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
}



