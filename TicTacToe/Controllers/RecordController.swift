//
//  RecordController.swift
//  TicTacToe
//
//  Created by Click Labs on 2/9/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class RecordController: UIViewController, UITableViewDelegate {
  
  @IBOutlet weak var taskTable: UITableView!
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->Int {
    return gameRecords.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->UITableViewCell   {   // setting the text label in the cell
    var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
    cell.textLabel?.adjustsFontSizeToFitWidth = true
    cell.textLabel?.text = gameRecords[indexPath.row]
    return cell
  }
  
  override func viewWillAppear(animated: Bool) { //setting the initial data load
    if var itemInMemory :AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("gameRecords"){
      gameRecords = []
      for i in 0..<itemInMemory.count {
        gameRecords.append(itemInMemory[i] as NSString)
      }
    }
    taskTable.reloadData()
  }
  
  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    // function for deleting the todo item
    if editingStyle == UITableViewCellEditingStyle.Delete {
      gameRecords.removeAtIndex(indexPath.row)
      tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
      taskTable.reloadData()
      let storedItem = gameRecords
      NSUserDefaults.standardUserDefaults().setObject(storedItem, forKey: "gameRecords")
      NSUserDefaults.standardUserDefaults().synchronize()
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //return the table height
    return tableView.rowHeight;
  }
  
  func colorForIndex(index: Int) -> UIColor { // user defined function for creating color
    let itemCount = gameRecords.count - 1
    let val = (CGFloat(index) / CGFloat(itemCount)) * 0.8
    return UIColor(red: 1.0, green: val , blue: 0.0, alpha: 0.6)
  }
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,forRowAtIndexPath indexPath: NSIndexPath) {
    // setting the different color  to each tablecell
    cell.backgroundColor = colorForIndex(indexPath.row)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}
