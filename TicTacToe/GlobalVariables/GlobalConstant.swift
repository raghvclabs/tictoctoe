//
//  GlobalConstant.swift
//  TicTacToe
//
//  Created by Click Labs on 3/2/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation

let WinningCombination = [ [0,1,2],[3,4,5,],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]] // array contain the winning Combination of the TicTacToe
var player1Name = "Player1"  //Global variable for player name
var player2Name = "Player2"
var turn = 1 // for choosing the choice whether cross or naught
var gameRecords :[String] = []    //Global array to store  score Records
